require "byebug"
# Sum
#
# Write an Array method, `sum`, that returns the sum of the elements in the
# array. You may assume that all of the elements are integers.

class Array
  def sum
    return 0 if self.length == 0
    reduce(&:+)
  end
end

# Square
#
# Write an array method, `square`, that returns a new array containing the
# squares of each element. You should also implement a "bang!" version of this
# method, which mutates the original array.

class Array
  def square!
    map! { |el| el ** 2 }
  end

  def square
    map { |el| el ** 2 }
  end
end

# Finding Uniques
#
# Monkey-patch the Array class with your own `uniq` method, called
# `my_uniq`. The method should return the unique elements, in the order
# they first appeared:
#
# ```ruby
# [1, 2, 1, 3, 3].my_uniq # => [1, 2, 3]
# ```
#
# Do not use the built-in `uniq` method!

class Array
  def my_uniq
    uniqs = []
    each { |el| uniqs << el unless uniqs.include?(el) }
    uniqs
  end
end

# Two Sum
#
# Write a new `Array#two_sum` method that finds all pairs of positions
# where the elements at those positions sum to zero.
#
# NB: ordering matters. I want each of the pairs to be sorted smaller
# index before bigger index. I want the array of pairs to be sorted
# "dictionary-wise":
#
# ```ruby
# [-1, 0, 2, -2, 1].two_sum # => [[0, 4], [2, 3]]
# ```
#
# * `[0, 2]` before `[1, 2]` (smaller first elements come first)
# * `[0, 1]` before `[0, 2]` (then smaller second elements come first)

class Array
  def two_sum
    pairs = []
    each_with_index do |first_el, i|
      self[i + 1...self.length].each_with_index do |second_el, k|
        j = k + i + 1
        pairs << [i, j] if first_el + second_el == 0
      end
    end
    pairs.sort do |x, y|
      if x.first != y.first
        x.first <=> y.first
      else
        x.last <=> y.last
      end
    end
  end
end

# Median
#
# Write a method that finds the median of a given array of integers. If
# the array has an odd number of integers, return the middle item from the
# sorted array. If the array has an even number of integers, return the
# average of the middle two items from the sorted array.

class Array
  def median
    return nil if length == 0
    if length.even?
      even_mid = length / 2
      result = (sort[even_mid - 1] + sort[even_mid]).to_f / 2
    else
      odd_mid = (length - 1) / 2
      result = sort[odd_mid]
    end
    result
  end
end

# My Transpose
#
# To represent a *matrix*, or two-dimensional grid of numbers, we can
# write an array containing arrays which represent rows:
#
# ```ruby
# rows = [
#     [0, 1, 2],
#     [3, 4, 5],
#     [6, 7, 8]
#   ]
#
# row1 = rows[0]
# row2 = rows[1]
# row3 = rows[2]
# ```
#
# We could equivalently have stored the matrix as an array of
# columns:
#
# ```ruby
# cols = [
#     [0, 3, 6],
#     [1, 4, 7],
#     [2, 5, 8]
#   ]
# ```
#
# Write a method, `my_transpose`, which will convert between the
# row-oriented and column-oriented representations. You may assume square
# matrices for simplicity's sake. Usage will look like the following:
#
# ```ruby
# matrix = [
#   [0, 1, 2],
#   [3, 4, 5],
#   [6, 7, 8]
# ]
#
# matrix.my_transpose
#  # => [[0, 3, 6],
#  #    [1, 4, 7],
#  #    [2, 5, 8]]
# ```
#
# Don't use the built-in `transpose` method!

#       [1, 2]
#       [3, 4]

# want: [1, 3]
#       [2, 4]

class Array
  def my_transpose
    return nil if length == nil || self[0].length == 0
    row_count = length
    col_count = self[0].length
    transposed_arr = Array.new(col_count) { Array.new(row_count) }
    # byebug

    ## I've already set up transposed_arr with correct dimensions;
    ## I now iterate through the original array and switch indices
    self.each_with_index do |row, row_index|
      row.each_with_index do |el, col_index|
        transposed_arr[col_index][row_index] = el
      end
    end

    transposed_arr
  end
end

# Bonus: Refactor your `Array#my_transpose` method to work with any rectangular
# matrix (not necessarily a square one).
